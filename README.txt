
### Role Contact README

Author: Andrew Turner (andrew@egressive.com) and Dave Lane (dave@egressive.com) at Egressive Ltd.  http://egressive.com

Description:  Egressive's Role Contact module combines a number of useful features:
1. It creates a page (themeable), at a administrator-selectable path,  which provides a list of users, with a summary profile view, in an administrator selected role, e.g. "company staff".  It also creates a (themeable) user profile page for each user in the list showing their full "staff" profile.  
  a. The order of presentation can be selected by a weights system, and defaults to alphabetical (based on profile surname if it exists).
  b. The summary fields are selected from any defined profile fields.
  c. The profile page fields are also selected from any defined profile fields.
  d. The administrator can provide text (via any applicable input filter) both above and below the list.
  e. By default, it respects profile fields which are marked as private.
  f. In both the summary and individual profile page views, provides a link to the contact form.
2. It creates a contact category for each user in the selected role, which is delineated from the rest of the categories.
  a. If the role is changed, the categories are re-created based on the members of the new role.
  b. It supercedes the "personal contact form" functionality provided by the contact module.
  c. Unlike the contact form's "personal contact form" functionality, it can be used by anonymous users.  It can and should probably be used in conjunction with the Captcha module to minimise contact form spam.
  d. The contact link on a Role Contact user profile or summary profile takes the user clicking it to the contact form with the selected user's category preselected.
      
Role Contact depends on these core modules:
1. profile
2. contact
3. path
4. filter  

Installation:  
1. Copy the tar archive in the Drupal modules directory (or sites/sitename/modules or sites/all/modules directory if used in multi-site mode) and untar using 
tar xfz rolecontact-5.x-1.dev.tar.gz 

2. Logged into the Drupal site as a user with suitable permissions, enable the module by going to Administer -> Site Building -> Modules - also enable the modules it depends on (they will be listed in the Module's description in the Modules list) to ensure full functionality.

3. Ensure that the relevant user roles have the ability to see the module and/or administer it by adjusting the Access Controls (Administer -> User Management -> Access Controls). 

Configuration:
1. Create (if it doesn't already exist) a role for the users you want to have appear in the Role Contact user list - might be something like "Our Staff" or similar

2. Go to Administer -> User Management -> Profiles and create the appropriate user Profile fields, particularly profile_firstname and profile_lastname (these will be used to create the relevant Contact form categories), for your users.

3. Go to the Role Contact administration page (Administer -> Site Configuration -> Role Contact) and select the appropriate role - also select the pathname for the summary user page, e.g. "staff_list" - in which case, you'd see the staff list by going to http://example.com/staff_list if your Drupal site was http://example.com.

4. You can also configure which fields to display in the summary and full profile views, a header and/or footer for the summary user page (e.g. with your company postal address, perhaps), and weights for users (if, for example, you want the company directors appear towards the top of the list).

Let us know if you like this module, and if not, how you've fixed it! 

An example of this module in action is available at 
http://egressive.com/people
